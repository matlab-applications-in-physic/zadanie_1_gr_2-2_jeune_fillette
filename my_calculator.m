%1

 h=6.62607015*10^(-34) %h is a Planck constant
 h/(2*pi)
 
 pause
 
 %2
 
 e=exp(1) % e is a Euler number
 sind(30/e) 
 
 pause
 
 %3
 
 hex2dec('123d3')/(2.455e23) % hex2dec convert hexadecimal number to decimal number
 
 pause
 
 %4
 
 sqrt(e-pi) % e is a Euler number
 
 pause
 
 %5
 
 p=num2str(pi, 20) % num2str converts pi number to string representation 'p' with the maximum number of digits equal 20
 p(12) % gives back 12 character of p
 
 pause
 
 %6
 
 datenum(date)-datenum('1999-10-08') 
 
 pause
 
 %7
 
 atan((e^(sqrt(7)/2)-log(6371/1e5))/hex2dec('aabb'))

 pause
 
 %8
 
 a=6.02214076e23
 a/(0.2e-006)*9
 
 pause
 
 %9
 
 (ans*2/9/100)/ans*1000
 
 pause